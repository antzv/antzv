--Расчет показателя КУТФОМС
WITH t AS (
	SELECT
		request_date :: DATE,
		NULL :: DATE                                        AS response_date,
		response_count :: BIGINT,
		transaction_id :: UUID,
		'Количество заявлений пользователей услуги "Сведения об оказанных медицинских услугах и их стоимости"' :: TEXT AS name,
		'1'::Text AS col_num

	FROM analytics.func_aggr_application_of_medical_services('2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_count BIGINT, transaction_id UUID)
	UNION ALL
	SELECT
		request_date :: DATE,
		response_date :: DATE,
		response_count :: BIGINT,
		transaction_id :: UUID,
		'в том числе исполненные' :: TEXT AS name,
		'3'::Text AS col_num
	FROM analytics.func_aggr_executed_medical_services('2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_date DATE, response_count BIGINT, transaction_id UUID)
	UNION ALL
	SELECT
		request_date :: DATE,
		response_date :: DATE,
		response_count :: BIGINT,
		transaction_id :: UUID,
		'в том числе отказ со стороны ГИС ОМС' :: TEXT AS name,
		'4a'::Text AS col_num
	FROM analytics.func_aggr_rejected_get_insured_regions_of_reject(
			     '2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_date DATE, response_count BIGINT, transaction_id UUID)
	UNION ALL
	SELECT
		request_date :: DATE,
		response_date :: DATE,
		response_count :: BIGINT,
		transaction_id :: UUID,
		'в том числе отказ со стороны ГИС ОМС' :: TEXT AS name,
		'4b'::Text AS col_num
	FROM analytics.func_aggr_rejected_get_insured_regions_of_data(
			     '2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_date DATE, response_count BIGINT, transaction_id UUID)
		--Описание выходных параметров SQL-функции для получения агрегированных данных по графе 5
   --в том числе отказ со стороны КУТФОМС
	/*

UNION ALL

--func_aggr_rejected_medical_services_cutfoms
SELECT
      request_date::date
     ,response_date::date
     ,response_count::bigint
     ,transaction_id::uuid
     ,'func_aggr_rejected_medical_services_cutfoms'::text AS name
FROM analytics.func_aggr_rejected_medical_services_cutfoms( '2014-01-01 23:59:59.999999'::timestamp without time zone)
    AS (request_date date, response_date date,response_count bigint,transaction_id uuid)
-----------------------------------------
*/
	UNION ALL
	SELECT
		request_date :: DATE,
		response_date :: DATE,
		response_count :: BIGINT,
		transaction_id :: UUID,
		'в том числе отказ со стороны ИС ТФОМС' :: TEXT AS name,
		'6'::Text AS col_num
	FROM analytics.func_aggr_rejected_medical_services('2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_date DATE, response_count BIGINT, transaction_id UUID)
	UNION ALL
	SELECT
		request_date :: DATE,
		NULL :: DATE                                       AS response_date,
		request_count :: BIGINT,
		transaction_id :: UUID,
		'Количество заявлений пользователей услуги "Подача заявления о выборе СМО"' :: TEXT AS name,
		'7'::Text AS col_num
	FROM analytics.func_aggr_application_of_medical_policy('2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, request_count BIGINT, transaction_id UUID)
	UNION ALL
	--Графа 8 сумма 9-10 строк
	SELECT
		request_date :: DATE,
		response_date :: DATE,
		response_count::BIGINT AS response_count,
		transaction_id :: UUID,
		'в том числе исполненные' :: TEXT AS name,
		'9'::Text AS col_num
	FROM analytics.func_aggr_executed_medical_policy('2014-01-01 23:59:59.999999' :: TIMESTAMP WITHOUT TIME ZONE)
		AS (request_date DATE, response_date DATE, response_count BIGINT, transaction_id UUID)
	--Описание выходных параметров SQL-функции для получения агрегированных данных по графе 10
	--в том числе отказ
	/*
UNION ALL

SELECT
      request_date::date
     ,response_date::date
     ,response_count::bigint
     ,transaction_id::uuid
     ,'func_aggr_all_rejected_medical_policy'::text AS name
FROM analytics.func_aggr_all_rejected_medical_policy( '2014-01-01 23:59:59.999999'::timestamp without time zone)
    AS (request_date date, response_date date, response_count bigint, transaction_id uuid)

 */
)
,tt AS (
    --все показатели кроме расчетных и аггрегатов
    SELECT request_date, sum(response_count)::Bigint AS response_count, name, col_num From t WHERE col_num != '4a' And col_num != '4b'
    GROUP BY 1,3,4
    UNION ALL
    --расчет показателя 2
    SELECT request_date, sum(response_count)::bigint AS response_count
         , 'Количество переданных ответов на ЕПГУ по результатам оказания услуги "Сведения об оказанных медицинских услугах и их стоимости"' AS name,
           '2' AS col_num From t WHERE col_num IN ('3','4a','4b','5','6') GROUP BY 1,3,4
    UNION ALL
    --расчет показателя 4
    SELECT
    request_date, sum(t.response_count)::bigint AS response_count, 'в том числе отказ со стороны ГИС ОМС' As name
         , '4' AS col_num  From t WHERE t.col_num = '4a' OR t.col_num = '4b' GROUP BY 1,3,4
    UNION ALL
  --  расчет показателя 8
    SELECT
    t.request_date, sum(t.response_count)::bigint AS response_count, 'Количество приглашений за полисом ОМС' AS name, '8' AS col_num
    From t WHERE t.col_num ='9' OR t.col_num = '10' GROUP BY 1,3,4
    )
	SELECT
	 sum(tt.response_count) As response_count
	,tt.name
	,tt.col_num
    ,(:tableName) as "tableName"
	,(:calcCode) as "calcCode"
	,(now()) as "sysdate"
	,(to_date(:dateTo,'yyyymmdd') - interval '1 day') as "ondate"
	,to_date(:dateFrom,'yyyymmdd') as "dateFrom"
	,to_date(:dateTo,'yyyymmdd') as "dateTo"
	,to_char(to_date(:dateFrom,'yyyymmdd'),'yyyy')::bigint as "period_year"
	,to_char(to_date(:dateFrom,'yyyymmdd'),'yyyy')::character varying as "period_yearstr"
	,to_char(to_date(:dateFrom,'yyyymmdd'),'mm')::bigint as "period_month"
	,to_char(to_date(:dateFrom,'yyyymmdd'),'mm')::bigint as "period_monthint"
	,to_date(:dateFrom,'yyyy0101')::date as "period_yearDateFrom"
	,to_date(:dateFrom,'yyyymmdd')::date as "period_yearDateTo"
	FROM tt
  WHERE tt.request_date >=  to_date(:dateFrom,'yyyymmdd')
       AND
        tt.request_date < to_date(:dateTo,'yyyymmdd')
 	GROUP BY 2,3,4,5,6,7,8,9,10,11,12,13,14,15
;

---1.07. Показатели
BEGIN;
  SELECT
     (:dateFrom) as "dateFrom"
    ,(:dateTo) as "dateTo"
    ,(:calcCode) as "calcCode"
    ,(:tableName) as "tableName"
    ,m."id"
    ,m."code"
    ,m."DetailRegion"
    ,m."typeValue"
    ,m."sqlQuery"
  FROM egisz_mears m
  LEFT JOIN egisz_mears_group gr on m."groupId" = gr."id"
  WHERE (nullif(nullif(lower(trim(:mear)),'-1'),'') is null OR lower(trim(m."code")) = lower(trim(:mear)))
    AND m."isActive" = true
    AND m."dateFrom"::date <= to_date((:dateFrom)::text,'yyyymmdd')::date
    AND m."dateTo"::date >= to_date((:dateTo)::text,'yyyymmdd')::date
    AND (
         gr.id is not null AND gr."isActive" = true AND gr."dateFrom"::date <= to_date((:dateFrom)::text,'yyyymmdd')::date
             AND gr."dateTo"::date >= to_date((:dateTo)::text,'yyyymmdd')::date
      OR gr.id is null
        )
  ORDER BY gr."numSort", gr."code", m."numSort", m."code"
  ;
COMMIT;

--1.08. Показатель Начало
--Задаем переменную

--1.09. Сбор показателей Основной блок расчетов
BEGIN;

create local temp table tmp_param on commit drop as
select
  coalesce((:typeValue)::bigint, 10::bigint)::bigint as "typeValue"
 ,(:sqlQuery)::character varying as "sqlQuery"
 ,to_date((:dateFrom)::character varying,'yyyymmdd')::date as "dateFrom"
 ,to_date((:dateTo)::character varying,'yyyymmdd')::date as "dateTo"

 ,(:dateFrom)::bigint                     as "nDateFrom"
 ,(:dateTo)::bigint                       as "nDateTo"

 ,coalesce((:DetailRegion)::boolean,false)::boolean as "DetailRegion"

 ,(:calcCode)                             as "dimPeriodMonthCode"
;

create local temp table tmp_egisz_data on commit drop as
select
   null::bigint as "region_id"
  ,null::character varying as "region_name"
  ,null::numeric as "value"
limit 0
;

do language plpgsql $$
declare
typeValue bigint;
sqlQuery text;
nDateFrom bigint;
nDateTo bigint;
dateFrom date;
dateTo date;
dimPeriodMonthCode character varying;
begin
  select t."typeValue"
        ,t."sqlQuery"
        ,t."dateFrom"
        ,t."dateTo"
        ,t."nDateFrom"
        ,t."nDateTo"
        ,t."dimPeriodMonthCode"
    INTO typeValue
        ,sqlQuery
        ,dateFrom
        ,dateTo
        ,nDateFrom
        ,nDateTo
        ,dimPeriodMonthCode
  FROM tmp_param as t;

  begin

    sqlQuery := 'insert into tmp_egisz_data ("region_id","region_name","value") '||sqlQuery;


        sqlQuery := replace(sqlQuery,'&lt;','<');
        sqlQuery := replace(sqlQuery,'&gt;','>');
        sqlQuery := replace(sqlQuery,'[{dateFrom}]','to_date('''||to_char(dateFrom,'yyyymmdd')||''',''yyyymmdd'')');

 sqlQuery := replace(sqlQuery,'[{dateTo}]'  ,'to_date('''||to_char(dateTo::date,'yyyymmdd')||''',''yyyymmdd'')');

 --sqlQuery := replace(sqlQuery,'[{dateTo}]', dateTo::date);

  sqlQuery := replace(sqlQuery,'[{dateFromYear}]','to_date('''||to_char(dateFrom,'yyyy0101')||''',''yyyymmdd'')');
        sqlQuery := replace(sqlQuery,'[{nDateFrom}]', nDateFrom::text);
        sqlQuery := replace(sqlQuery,'[{nDateTo}]'  , nDateTo::text);
        sqlQuery := replace(sqlQuery,'[{nDateFromYear}]', (to_char(dateFrom,'yyyy0101'))::text);

        sqlQuery := replace(sqlQuery,'[{dimPeriodMonthCode}]', dimPeriodMonthCode::text);

    execute sqlQuery;
--  exception when others then null;
  end;

end;
$$;

SELECT
   m."onDate"::bigint as "onDate"
  ,m."dimPeriodMonthId"
  ,m."mear"::bigint as "mear"
  ,coalesce(si.id::bigint,0::bigint)::bigint as "region"
  ,now() as "createddatetime"
  ,case when coalesce((:typeValue)::bigint,  10::bigint)::bigint in ( 10::bigint, 30::bigint, 40::bigint)
        then coalesce( t."value"::numeric, 0::numeric )::numeric else null::numeric end as "value"
  ,case when coalesce((:typeValue)::bigint,  10::bigint)::bigint = 20::bigint

-- then coalesce( t."value"::numeric, 0::numeric )::numeric else null::numeric end as "uniqueValue"

then ( t."value"::numeric) else null::numeric end as "uniqueValue"

FROM (
  SELECT
     (:dateFrom)::bigint as "onDate"
    ,(:mear_id )::bigint "mear"
    ,(d.id) as "dimPeriodMonthId"
  FROM dimPeriodMonth as d
  WHERE d."id" = left((:dateFrom)::text,6)::bigint
) as m
LEFT JOIN tmp_egisz_data as t ON true
LEFT JOIN sub_ind si ON si.id != 0::bigint AND si."fnsicode" = t."region_id"

UNION ALL

SELECT
   m."onDate"::bigint as "onDate"
  ,m."dimPeriodMonthId"
  ,m."mear"::bigint as "mear"
  ,coalesce(si.id::bigint,0::bigint)::bigint as "region"
  ,now() as "createddatetime"
  ,null::numeric as "value"
  ,null::numeric as "uniqueValue"
FROM (
  SELECT
     (:dateFrom)::bigint as "onDate"
    ,(:mear_id )::bigint "mear"
    ,(d.id) as "dimPeriodMonthId"
  FROM dimPeriodMonth as d
    WHERE d."id" = left((:dateFrom)::text,6)::bigint ) as m
JOIN sub_ind si ON si.id != 0::bigint
WHERE NOT EXISTS( SELECT null FROM tmp_egisz_data as t WHERE si."fnsicode" = t."region_id" )
  AND coalesce((:DetailRegion)::boolean,false)::boolean = true
;
COMMIT;