WITH
    table_ses_begin AS (
    SELECT
      user_id, happened_at, page,
      CASE page
          WHEN 'rooms.homework-showcase' THEN 1
          WHEN 'rooms.view.step.content' THEN 2
          WHEN 'rooms.lesson.rev.step.content' THEN 3
          ELSE 0
      END ses_page,
      CASE
      -- если страница первая, что при вычитании получится нуль, поэтому ставим 4000, отсюда срабатывает условие первой страницы
        WHEN COALESCE(EXTRACT('epoch' FROM happened_at - LAG(happened_at) OVER(partition BY user_id ORDER BY happened_at)),4000) < 3600
          THEN 0 
          ELSE 1
      END ses_time_begin
    FROM test.vimbox_pages
) , split_to_ses AS (
SELECT
 *
 -- оконной функцией нарастающего итога нумеруем сесии у пользователей
 ,sum(ses_time_begin) over (partition BY user_id order by happened_at rows between unbounded preceding and current row) AS ses_time
FROM table_ses_begin
), calc_ses AS (
SELECT 
user_id, ses_time
--формируем списки номеров страниц и времени посещения
, listagg(ses_page, ',') within group (order by happened_at)  as array_pages 
, listagg(happened_at, ',') within group (order by happened_at)  as array_time
-- из списка времени берем первый и последний элементы тем самым определяя начало и конец сесии
, split_part(array_time, ',', 1)::timestamp as begin
, reverse(split_part(reverse(array_time), ',', 1))::timestamp + INTERVAL '1 hour' as endd 
FROM split_to_ses
-- группируем сессии по пользователю и номеру сессии
  group by user_id, ses_time
  having strpos(array_pages, '1') != 0 and strpos(array_pages, '2') != 0 and strpos(array_pages, '3') != 0
         and (strpos(array_pages, '1') < strpos(array_pages, '2') and strpos(array_pages, '2') < strpos(array_pages, '3') and strpos(array_pages, '1') < strpos(array_pages, '3'))
  order by user_id
)
SELECT 
  user_id, begin, endd
, date_part('hour', begin) AS ses_date_part
, EXTRACT(minute FROM (endd - begin)) as duration_ses
FROM calc_ses