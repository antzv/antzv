WITH t AS (
    --��������� ������������������ ���
SELECT
       gs::date AS date
FROM generate_series('2020-01-01', '2020-12-31', interval '1 day') as gs)
, t2 AS (
    --���������� �������������� �����
    SELECT date
         , extract(YEAR from date)  AS year
         , extract(QTR from date)   AS Quarter
         , extract(MONTH from date) AS month_num
         , extract(MONTH from date) AS month       --name
         , extract(DAY from date)   AS day
         , extract(WEEK from date)  AS weeks
         , extract(DOW from date)   AS day_of_week_num
         , extract(DOW from date)   AS day_of_week --name
    FROM t
)
    --���������� �������
SELECT
       date, year, Quarter, month_num, day, weeks, day_of_week_num
,CASE month
    WHEN 1  THEN '������'
    WHEN 2  THEN '�������'
    WHEN 3  THEN '����'
    WHEN 4  THEN '������'
    WHEN 5  THEN '���'
    WHEN 6  THEN '����'
    WHEN 7  THEN '����'
    WHEN 8  THEN '������'
    WHEN 9  THEN '��������'
    WHEN 10 THEN '�������'
    WHEN 11 THEN '������'
    WHEN 12 THEN '�������'
END AS months
,Case day_of_week
    WHEN 0 THEN '�����������'
    WHEN 1 THEN '�����������'
    WHEN 2 THEN '�������'
    WHEN 3 THEN '�����'
    WHEN 4 THEN '�������'
    WHEN 5 THEN '�������'
    WHEN 6 THEN '�������'
END AS day_of_week
FROM t2